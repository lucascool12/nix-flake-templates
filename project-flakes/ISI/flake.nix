{
  description = "Application packaged using poetry2nix";

  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";
  inputs.unstable.url = "github:nixos/nixpkgs/nixos-unstable";
  inputs.nixold.url = "https://github.com/NixOS/nixpkgs/archive/f597e7e9fcf37d8ed14a12835ede0a7d362314bd.tar.gz";

  outputs = { self, nixpkgs, nixold, unstable, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        # see https://github.com/nix-community/poetry2nix/tree/master#api for more functions and examples.
        # inherit (poetry2nix.legacyPackages.${system}) mkPoetryApplication;
        # pkgs = nixpkgs.legacyPackages.${system};
        pkgs = import nixpkgs {
          inherit system;
          config = {
            permittedInsecurePackages = [
              "python-2.7.18.6"
            ];
          };
        };
        pkgsold = import nixold {
          inherit system;
          config = {
            permittedInsecurePackages = [
              "nodejs-12.22.12"
            ];
          };
        };
        unstablepkgs = import unstable { inherit system; };
      in
      {
        devShells.default = pkgs.mkShell {
          name = "idpDevShell";
          venvDir = "./.venv";
          packages = with pkgs; [
            unstablepkgs.python311Packages.pip
            unstablepkgs.python311
            unstablepkgs.python311Packages.virtualenv
            unstablepkgs.python311Packages.venvShellHook
            unstablepkgs.z3_4_12
            unstablepkgs.z3_4_12.python
            poetry
            pkgsold.nodePackages."@angular/cli"
            pkgsold.nodejs-12_x
          ];
          postVenvCreation = ''
            unset SOURCE_DATE_EPOCH
            poetry install
          '';

          postShellHook = ''
            # export LD_LIBRARY_PATH="${unstablepkgs.stdenv.cc.cc.lib}/lib/"
            unset SOURCE_DATE_EPOCH
          '';
        };
      });
}
