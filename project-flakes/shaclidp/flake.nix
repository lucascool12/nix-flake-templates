{
  description = "A basic flake with a shell";
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      devShells.default = pkgs.mkShell {
        name = "shidpDevShell";
        venvDir = "./.venv";
        packages = with pkgs; [
          python311
          python311Packages.venvShellHook
          python311Packages.virtualenv
        ];
        postVenvCreation = ''
          unset SOURCE_DATE_EPOCH
          pip install -e .
        '';

        postShellHook = ''
          export LD_LIBRARY_PATH=${pkgs.stdenv.cc.cc.lib}/lib/
          unset SOURCE_DATE_EPOCH
        '';
      };
    });
}
