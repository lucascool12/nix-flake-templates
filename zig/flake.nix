{
  description = "Application packaged using poetry2nix";

  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.unstable.url = "github:nixos/nixpkgs/nixos-unstable";

  outputs = { self, unstable, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        unstablepkgs = import unstable { inherit system; };
      in
      {
        devShells.default = unstablepkgs.mkShell {
          packages = with unstablepkgs; [
            zig
            zls
          ];
        };
      });
}
